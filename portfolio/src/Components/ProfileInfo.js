import React, { useEffect, useState } from 'react';
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';

const ProfileInfo = () => {
    const [myanmarPercent, setMyanmarPercentage] = useState(0);
    const [englishPercent, setEnglishPercentage] = useState(0);

    useEffect(() => {
        setTimeout(() => {
          if (myanmarPercent < 100) {
            setMyanmarPercentage(myanmarPercent + 1);
          }
          if (englishPercent < 70) {
            setEnglishPercentage(englishPercent + 1);
          }
        }, 30);
      }, [myanmarPercent, englishPercent]);


    return (
        <div className="basis-[22.5%]">
            <div className="profile-info mx-auto">
                <div className="test">
                    <div className="sticky z-50">
                        <div className="art-avatar">
                            <a data-no-swup="" href="https://bslthemes.com/arter/wp-content/uploads/2020/09/face-1-min.jpg" class="art-avatar-curtain">
                                <img className="text-center m-auto" src="https://bslthemes.com/arter/wp-content/uploads/2020/09/face-1-small.jpg" alt="avatar"></img>
                                <i class="fas fa-expand"></i>
                            </a>
                        </div>
                        

                        <h5 className="art-name mt-3 mb-3 text-base text-white text-center"><a href="#" className="no-underline">Sai Kaung Khant Zaw</a></h5>

                        <div className="art-sm-text text-sm text-center">
                            Front-end Deweloper<br></br>UI/UX Designer
                        </div>
                    </div>

                    {/* address */}
                    <div className="address-box mt-5 ml-5">
                        <ul>
                            <li className="text-white">Residence:   <span className="absolute right-0 mr-5">Myanmar</span></li>
                            <li className="text-white">City<span className="absolute right-0 mr-5">Yangon</span></li>
                            <li className="text-white">Age<span className="absolute right-0 mr-5">27</span></li>
                        </ul>
                        <br></br>
                    </div>
                    <hr className="mx-auto mb-3 text-grey" width="85%"></hr>

                    {/* languages box  */}
                    <div className="languages-box mt-5 ml-5">
                        <h5 className="text-white mb-5">Languages</h5>
                        <div className="flex justify-around mr-5 mb-5">
                            <div className=''>
                                <div className="lang-progress">
                                    <CircularProgressbar 
                                        value={myanmarPercent}
                                        text={`${myanmarPercent}%`}
                                        styles={buildStyles({
                                            pathColor: `rgb(254, 193, 7, ${myanmarPercent / 100})`,
                                            trailColor: '#000',
                                            textColor: '#fff',

                                            })}
                                        className="mb-2"/>
                                    <h4 className='text-white'>Myanmar</h4>
                                </div>
                            </div>
                            <div className=''>
                                <div className="lang-progress">
                                    <CircularProgressbar 
                                        value={englishPercent} 
                                        text={`${englishPercent}%`} 
                                        styles={buildStyles({
                                            pathColor: `rgb(254, 193, 7, ${englishPercent / 70})`,
                                            trailColor: '#000',
                                            textColor: '#fff',
                                            })}
                                        className="mb-2"
                                    />
                                    <h4 className='text-white'>English</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr className="mx-auto mb-3 text-grey" width="85%"></hr>

                    {/* skill box */}
                    <div className="skill-box m-5">
                        <h5 className="text-white mb-5">Coding</h5>
                        <div className="skill mb-5">
                            <h6>HTML <span className="absolute right-0 mr-5">95%</span></h6>
                            <div className="progress progress-moved">
                                <div className="progress-bar" >
                                </div>                       
                            </div> 
                        </div>
                        <div className="skill mb-5">
                            <h6>HTML <span className="absolute right-0 mr-5">95%</span></h6>
                            <div className="progress progress-moved">
                                <div className="progress-bar" >
                                </div>                       
                            </div> 
                        </div>
                        <div className="skill mb-5">
                            <h6>HTML <span className="absolute right-0 mr-5">95%</span></h6>
                            <div className="progress progress-moved">
                                <div className="progress-bar" >
                                </div>                       
                            </div> 
                        </div>
                        <div className="skill mb-10">
                            <h6>HTML <span className="absolute right-0 mr-5">95%</span></h6>
                            <div className="progress progress-moved">
                                <div className="progress-bar" >
                                </div>                       
                            </div> 
                        </div>
                    </div>
                    <hr className="mx-auto mb-3 text-grey" width="85%"></hr>

                    {/* knowledge */}
                    <div className="knowledge-box m-5">
                        <h5 className="text-white mb-5">Knowledge</h5>
                        <ul className="text-white p-15">
                            <li className='flex text-fontgrey'>
                                <svg className="text-red-500 mr-3 text-orange"  width="15" height="15" viewBox="0 0 24 14" stroke-width="3" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">  <path stroke="none" d="M0 0h24v24H0z"/>  <path d="M5 12l5 5l10 -10" /></svg>
                                Bootstrap, Materialize
                            </li>
                            <li className='flex text-fontgrey'>
                                <svg className="text-red-500 mr-3 text-orange"  width="15" height="15" viewBox="0 0 24 14" stroke-width="3" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">  <path stroke="none" d="M0 0h24v24H0z"/>  <path d="M5 12l5 5l10 -10" /></svg>
                                Stylus, Sass, Less
                            </li>
                            <li className='flex text-fontgrey'>
                                <svg className="text-red-500 mr-3 text-orange"  width="15" height="15" viewBox="0 0 24 14" stroke-width="3" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">  <path stroke="none" d="M0 0h24v24H0z"/>  <path d="M5 12l5 5l10 -10" /></svg>
                                Gulp, Webpack, Grunt
                            </li>
                            <li className='flex text-fontgrey'>
                                <svg className="text-red-500 mr-3 text-orange"  width="15" height="15" viewBox="0 0 24 14" stroke-width="3" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">  <path stroke="none" d="M0 0h24v24H0z"/>  <path d="M5 12l5 5l10 -10" /></svg>
                                GIT knowledge
                            </li>
                        </ul>
                    </div>
                    <hr className="mx-auto mb-3 text-grey" width="85%"></hr>
                    
                    {/* CV */}
                    <div className="knowledge-box m-5">
                        <h5 className="flex text-white mb-5 text-fontgrey">DOWNLOAD CV
                            <svg className="text-red-500 ml-2 mt-1"  width="15" height="15" viewBox="0 0 24 24" stroke-width="3" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">  <path stroke="none" d="M0 0h24v24H0z"/>  <path d="M4 17v2a2 2 0 0 0 2 2h12a2 2 0 0 0 2 -2v-2" />  <polyline points="7 11 12 16 17 11" />  <line x1="12" y1="4" x2="12" y2="16" /></svg>
                        </h5>
                    </div>
                    
                    <div className="portfolio-box-footer flex justify-around text-fontgrey pt-2 pr-10 pl-10 z-50">
                        <svg class="h-4 w-4 text-red-500"  width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">  <path stroke="none" d="M0 0h24v24H0z"/>  <path d="M9 19c-4.286 1.35-4.286-2.55-6-3m12 5v-3.5c0-1 .099-1.405-.5-2 2.791-.3 5.5-1.366 5.5-6.04a4.567 4.567 0 0 0 -1.333 -3.21 4.192 4.192 0 00-.08-3.227s-1.05-.3-3.476 1.267a12.334 12.334 0 0 0 -6.222 0C6.462 2.723 5.413 3.023 5.413 3.023a4.192 4.192 0 0 0 -.08 3.227A4.566 4.566 0 004 9.486c0 4.64 2.709 5.68 5.5 6.014-.591.589-.56 1.183-.5 2V21" /></svg>
                        <svg class="h-4 w-4 text-red-500"  width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">  <path stroke="none" d="M0 0h24v24H0z"/>  <path d="M7 10v4h3v7h4v-7h3l1 -4h-4v-2a1 1 0 0 1 1 -1h3v-4h-3a5 5 0 0 0 -5 5v2h-3" /></svg>
                        <svg class="h-4 w-4 text-red-500"  width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">  <path stroke="none" d="M0 0h24v24H0z"/>  <path d="M22 4.01c-1 .49-1.98.689-3 .99-1.121-1.265-2.783-1.335-4.38-.737S11.977 6.323 12 8v1c-3.245.083-6.135-1.395-8-4 0 0-4.182 7.433 4 11-1.872 1.247-3.739 2.088-6 2 3.308 1.803 6.913 2.423 10.034 1.517 3.58-1.04 6.522-3.723 7.651-7.742a13.84 13.84 0 0 0 .497 -3.753C20.18 7.773 21.692 5.25 22 4.009z" /></svg>
                        <svg class="h-4 w-4 text-red-500"  viewBox="0 0 24 24"  fill="none"  stroke="currentColor"  stroke-width="2"  stroke-linecap="round"  stroke-linejoin="round">  <path d="M16 8a6 6 0 0 1 6 6v7h-4v-7a2 2 0 0 0-2-2 2 2 0 0 0-2 2v7h-4v-7a6 6 0 0 1 6-6z" />  <rect x="2" y="9" width="4" height="12" />  <circle cx="4" cy="4" r="2" /></svg>
                        <svg class="h-4 w-4 text-red-500"  width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">  <path stroke="none" d="M0 0h24v24H0z"/>  <path d="M15 10l-4 4l6 6l4 -16l-18 7l4 2l2 6l3 -4" /></svg>
                    </div>
                </div>
            </div>
        </div>

    );
}

export default ProfileInfo;