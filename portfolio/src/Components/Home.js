import React, { useEffect, useState } from 'react';
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';

import ProfileInfo  from './ProfileInfo';

const Home = () => {
    return (
        <div className="container">
            <div className="portfolio-box  min-w-full max-h-full">
                <div className="flex flex-row">
                    <ProfileInfo/>
                    <div class="basis-[70.5%]">02</div>
                    <div class="basis-[5%]">
                        <div className="side-menu"></div>
                    </div>
                </div>

            </div>
        </div>
    );
}

export default Home;