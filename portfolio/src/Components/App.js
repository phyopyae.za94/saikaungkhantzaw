import React from 'react';
import { BrowserRouter as Router,Routes, Route } from 'react-router-dom';

import Home from './Home';

const App = () => {

    return(
        <div className="app">
            <main>
            <Router>
                <Routes>
                    <Route path="/" exact element={<Home/>}/>
                </Routes>
            </Router>
                
            </main>
        </div>
    );
};

export default App;